(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

open Utils
open Types

(*
let loop_items values loop =
  let items = loop |> Soup.child_element in
  match items with
  | Some items when items |> Soup.name = Const.key_items ->
    let process_item loop item =
      let container = temp_container Const.key_items in
      wrap_with ~ifnot:container container item;
      let parent = item |> Soup.R.parent in
      let process_values loop value =
        let element = create_element_from item in
        Soup.append_child element (Soup.create_text value);
        Soup.append_child parent element;
        loop
      in
      let loop = List.fold_left process_values loop values in
      Soup.delete item;
      Soup.unwrap parent;
      loop
    in
    process_item loop items
  | _ -> loop
;;
*)

let process_valued_items node values =
  let items = Soup.(node $$ Const.selector_item_indiced) in
  Soup.iter
    (fun item ->
      match Soup.attribute Const.attr_index item with
      | Some index ->
        let index = Str.string_after index 1 |> int_of_string in
        let value = List.nth values index in
        ignore (Utils.replace_node_as_string item (vbasic_to_string value))
      | None -> ())
    items
;;

(*
let process_items_current item value =
  Utils.insert_after_node_as_data item (vbasic_to_string value)
;;
*)
let process_index_item index item =
  let indices = Soup.(item $$ Const.selector_item_indiced) in
  let apply index item =
    match Soup.attribute Const.attr_index item with
    | Some value ->
      if value = Const.value_index_current
      then (
        let stamp = Printf.sprintf "@%d" index in
        Soup.set_attribute Const.attr_index stamp item)
      else ()
    | None -> ()
  in
  if Soup.name item = Const.key_item
  then apply index item
  else Soup.iter (apply index) indices
;;

let process_items_key_values node value =
  let values =
    match value with
    | TValues value -> value
    | _ -> []
  in
  let children = Soup.elements (Soup.children node) in
  List.iteri
    (fun index _value ->
      Soup.iter
        (fun child ->
          let newnode = Utils.clone_element child in
          process_index_item index newnode;
          Soup.append_child node newnode)
        children)
    values;
  Soup.iter (fun child -> Soup.delete child) children;
  process_valued_items node values
;;

let process_loops globals node =
  wrap_with (temp_container Const.key_loop) node;
  let key, value =
    Soup.attribute Const.attr_key node, Soup.attribute Const.attr_value node
  in
  (match key, value with
   | Some _key, Some value -> process_items_key_values node (Utils.coerce_value value)
   | Some key, None ->
     (match Hashtbl.find_opt globals key with
      | Some value -> process_items_key_values node value
      | None -> ())
   | None, Some value -> process_items_key_values node (Utils.coerce_value value)
   | None, None -> ());
  (match Soup.parent node with
   | Some parent -> Soup.unwrap parent
   | None -> ());
  Soup.unwrap node
;;
