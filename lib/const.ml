(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let attr_class = "class"
let attr_value = "value"
let attr_result = "result"
let attr_key = "key"
let attr_index = "index"
let tag_def_container = "div"
let key_get = "get"
let key_set = "set"
let key_include = "include"
let key_md = "md"
let key_items = "items"
let key_loop = "loop"
let key_item = "item"

(*let selector_item_current = Printf.sprintf {|%s[%s^="_"]|} key_item attr_index*)
let selector_item_indiced = Printf.sprintf {|%s[%s^="@"]|} key_item attr_index
let value_index_current = "@"
let limit = 100
let wrapper_tag = "temp-container"
let list_sep = "|"
