open Utils

let resolve_macros globals content =
  let all_tag, all_key, all_value = "\\w+", "\\w+", "[\\w\\/$-_]+" in
  let pattern ?(use_delim = true) tag key value =
    let delim = if use_delim then {|["|']|} else "" in
    Utils.compile_regex
      (Printf.sprintf {|%s<(%s)\s+(%s)=["|'](%s)['|"]\s+\/>%s|} delim tag key value delim)
  in
  List.fold_left
    (fun _content matches ->
      let m = Re.Group.get matches in
      let _macro, tag, key, value = m 0, m 1, m 2, m 3 in
      match tag, key with
      | _, _ when (tag, key) = (Const.key_get, Const.attr_key) ->
        (match Hashtbl.find_opt globals value with
         | Some data ->
           Utils.replace_macro
             (pattern Const.key_get Const.attr_key value)
             content
             (Utils.value_to_string data)
         | None -> content)
      | _ -> content)
    content
    (Re.all (pattern all_tag all_key all_value) content)
;;

let resolve_getters globals xml =
  let nodes = Soup.(xml $$ Const.key_get) in
  let process xml node =
    let k = node |> Soup.R.attribute Const.attr_key in
    match Hashtbl.find_opt globals k with
    | Some data ->
      ignore (Utils.replace_node_as_string node (value_to_string data));
      xml
    | None -> xml
  in
  Soup.fold process xml nodes
;;

let resolve_setters ?globals ?(prealloc = 10) xml =
  let sets = Soup.(xml $$ Const.key_set) in
  let table =
    match globals with
    | Some g -> g
    | None -> Hashtbl.create prealloc
  in
  let process table node =
    ignore (store_node node table);
    Soup.delete node;
    table
  in
  Soup.fold process table sets, xml
;;

let resolve_includes ?(prealloc = 6) ~reclimit globals xml =
  let table = Hashtbl.create prealloc in
  let clean_recursion fname fragment =
    let nodes = Soup.(fragment $$ Const.key_include) in
    let fn i = i |> Soup.R.attribute Const.attr_value = fname in
    delete_nodes fragment (Soup.filter fn nodes)
  in
  let push inclusion =
    let fname = inclusion |> Soup.R.attribute Const.attr_value in
    match Hashtbl.find_opt table fname with
    | Some fragtext -> Soup.replace inclusion (parse_xml fragtext)
    | None ->
      let fragtext = read_file fname in
      let cleaned = clean_recursion fname (parse_xml fragtext) in
      Hashtbl.add table fname (to_string cleaned);
      Soup.replace inclusion cleaned
  in
  let rec process xml ?(counter = 0) limit =
    let _, xml = resolve_setters ~globals xml in
    let nodes = Soup.(xml $$ Const.key_include) in
    if counter >= limit
    then delete_nodes xml nodes
    else if Soup.count nodes > 0
    then (
      Soup.iter push nodes;
      process xml limit ~counter:(succ counter))
    else xml
  in
  process xml reclimit
;;

let resolve_markdown xml =
  let mds = Soup.(xml $$ Const.key_md) in
  let process xml md =
    let source =
      md
      |> Soup.texts
      |> String.concat "\n"
      |> String.trim
      |> Omd.of_string
      |> Omd.to_html
      |> Soup.parse
    in
    let element = create_element_from md in
    Soup.append_child element source;
    Soup.replace md element;
    xml
  in
  Soup.fold process xml mds
;;

let resolve_loops globals xml =
  Soup.iter (Loops.process_loops globals) Soup.(xml $$ Const.key_loop);
  xml
;;

let render
  ?vars
  ?postprocess
  ?(dump = false)
  ?(pretty = false)
  ?(compact = false)
  ~reclimit
  xml
  =
  let globals, xml = resolve_setters xml in
  let () =
    match vars with
    | Some vars ->
      let key k = Printf.sprintf "/%s" k in
      Hashtbl.iter (fun k v -> Hashtbl.add globals (key k) v) (assocs vars)
    | None -> ()
  in
  let result =
    xml
    |> resolve_includes ~reclimit globals
    |> resolve_getters globals
    |> resolve_loops globals
    |> resolve_getters globals
    |> resolve_markdown
  in
  ignore (if dump then Utils.print_globals globals else ());
  let htmlp content = Utils.to_string content in
  let compactp content = if compact then Utils.compact_string content else content in
  let prettyp content = if pretty then Utils.prettify content else content in
  let html = result |> htmlp |> resolve_macros globals |> compactp |> prettyp in
  match postprocess with
  | Some fn -> fn html
  | _ -> html
;;

let render_file
  ?(pretty = true)
  ?(compact = true)
  ?(reclimit = 25)
  ?(vars = [])
  ?(dump = false)
  ?(to_stdout = false)
  ~infile
  ~outfile
  ()
  =
  let content =
    Utils.parse_file infile |> render ~reclimit ~vars ~pretty ~compact ~dump
  in
  if to_stdout then Printf.printf "%s\n%!" content else Utils.write_file outfile content
;;
