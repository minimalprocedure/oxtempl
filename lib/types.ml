(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

 type tvaluebasic =
 | TValueInt of int
 | TValueFloat of float
 | TValueString of string

type tvalue =
 | TValue of tvaluebasic
 | TValues of tvaluebasic list

type tvalue_list = tvalue list