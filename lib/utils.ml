(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

open Types

let compile_regex regex_str = Re.Perl.re ~opts:[ `Multiline ] regex_str |> Re.compile

let compact_string content =
  let pattern = compile_regex " {2}|\n" in
  Re.replace_string ~all:true pattern ~by:"" content
;;

let replace_macro pattern content subst =
  Re.replace_string ~all:true pattern ~by:subst content
;;

let parse_xml text =
  Markup.string text
  |> Markup.parse_xml (*~report:(fun location error ->
    Markup.Error.to_string ~location error |> print_endline)*) 
  |> Markup.signals
  |> Soup.from_signals
;;

let parse_html = Soup.parse

let read_file fname = Soup.read_file fname
let parse_file fname = read_file fname |> parse_xml

let write_file file text =
  let out = Out_channel.open_text file in
  Out_channel.output_string out text;
  Out_channel.close out
;;

let to_pretty_string node =
  let html = Soup.to_string node in
  Soup.pretty_print (parse_html html)
;;

let prettify content =
  Soup.pretty_print (parse_html content)
;;

let to_string = Soup.to_string

let attributes node =
  let process acc name value =
    if name = Const.attr_key || name = Const.attr_value then acc else (name, value) :: acc
  in
  Soup.fold_attributes process [] node
;;

let delete_nodes xml nodes =
  Soup.fold
    (fun x n ->
      Soup.delete n;
      x)
    xml
    nodes
;;

let assocs assoc = Hashtbl.of_seq (List.to_seq assoc)

let wrap_with tag ?(attributes = []) ?ifnot node =
  let wrap () = Soup.wrap node (Soup.create_element tag ~attributes) in
  match ifnot with
  | Some ifnot ->
    let parent = node |> Soup.R.parent in
    if parent |> Soup.name = ifnot then () else wrap ()
  | None -> wrap ()
;;

let add_global globals key value = Hashtbl.add globals key value
let temp_container suffix = Printf.sprintf "%s-%s" Const.wrapper_tag suffix

let create_element_from ?id ?inner_text item =
  let tag =
    match item |> Soup.attribute Const.attr_key with
    | Some tag -> tag
    | _ -> Const.tag_def_container
  in
  let attrs = item |> attributes in
  Soup.create_element ?id ?inner_text ~attributes:attrs tag
;;

let split_to_list liststr =
  List.map (fun s -> String.trim s) (String.split_on_char Const.list_sep.[0] liststr)
;;

let coerce_value value =
  let to_int v =
    try Some (TValueInt (int_of_string v)) with
    | _ -> None
  in
  let to_float v =
    try Some (TValueFloat (float_of_string v)) with
    | _ -> None
  in
  let coerce value =
    match to_int value with
    | Some i -> i
    | None ->
      (match to_float value with
       | Some f -> f
       | None -> TValueString value)
  in
  let values = split_to_list value in
  match values with
  | [] -> TValue (TValueString "")
  | value :: [] -> TValue (coerce value)
  | _ ->
    let values = List.map (fun v -> coerce v) values in
    TValues values
;;

let vbasic_to_string = function
  | TValueInt i -> string_of_int i
  | TValueFloat f -> string_of_float f
  | TValueString s -> s
;;

let vlist_to_string values =
  List.fold_left (fun acc s -> vbasic_to_string s :: acc) [] values
  |> List.rev
  |> String.concat Const.list_sep
;;

let value_to_string = function
  | TValue value -> vbasic_to_string value
  | TValues values -> vlist_to_string values
;;

let unbox_list ?(default = []) = function
  | TValues v -> v
  | _ -> default
;;

let unbox_string ?(default = "") = function
  | TValueString v -> v
  | _ -> default
;;

let unbox_int ?(default = 0) = function
  | TValueInt v -> v
  | _ -> default
;;

let unbox_float ?(default = 0.) = function
  | TValueFloat v -> v
  | _ -> default
;;

let store_value ?(namespace = "") store key value =
  let coerced = coerce_value value in
  let nspace = if namespace <> String.empty then Printf.sprintf "%s." namespace else "" in
  Hashtbl.replace store (Printf.sprintf "$%s%s" nspace key) coerced;
  coerced
;;

let clone node = Soup.parse (Soup.to_string node)

let store_node ?(namespace = "") node store =
  let key = node |> Soup.R.attribute Const.attr_key in
  let value = node |> Soup.R.attribute Const.attr_value in
  store_value ~namespace store key value
;;

let replace_node node newnode =
  Soup.replace node newnode;
  newnode
;;

let replace_opt_node node newnode =
  match node with
  | Some node ->
    Soup.replace node newnode;
    Some newnode
  | None -> None
;;

let opt_node_to_soupnode_opt node =
  match node with
  | Some node ->
    let cloned = clone node in
    Soup.replace node cloned;
    Some cloned
  | None -> None
;;

let replace_node_as_string node data = replace_node node (Soup.parse data)
let replace_node_as_string2 node data = ignore (replace_node node (Soup.parse data))

let insert_after_node_as_data node data =
  let cloned = clone node in
  Soup.insert_after node cloned;
  ignore (replace_node_as_string node data);
  cloned
;;

let append_child_as_data node data =
  let child = Soup.parse data in
  Soup.append_child node child;
  child
;;

let append_child_as_data2 node data = ignore (append_child_as_data node data)

let print_globals globals =
  print_endline "---------------------------------------------------------";
  print_endline " VARIABLES ";
  print_endline "---------------------------------------------------------";
  Hashtbl.iter
    (fun key value -> Printf.printf "%s -> %s\n" key (value_to_string value))
    globals;
  print_endline "---------------------------------------------------------\n"
;;

let apply_if_element node fn =
  match Soup.element node with
  | Some node ->
    print_endline (Soup.to_string node);
    fn node
  | None -> ()
;;

let clone_element elem =
  let attributes = Soup.fold_attributes (fun acc k v -> (k, v) :: acc) [] elem in
  let children = Soup.children elem in
  let tag = Soup.name elem in
  let newelement = Soup.create_element tag ~attributes in
  Soup.fold
    (fun el child ->
      Soup.append_child el (clone child);
      el)
    newelement
    children
;;
