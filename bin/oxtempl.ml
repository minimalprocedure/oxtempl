open Oxtmpl

let infile = ref ""
let outfile = ref ""
let varsfile = ref ""
let pretty = ref true
let compact = ref true
let def_ireclim = 25
let ireclim = ref def_ireclim
let dump = ref false
let to_stdout = ref false

let usage =
  "oxtempl usage: \n"
  ^ Sys.argv.(0)
  ^ "\n"
  ^ "   --in filename \n"
  ^ "   --out filename \n"
  ^ "   --stdout filename \n"
  ^ "   --vars filename \n"
  ^ "   --no-pretty \n"
  ^ "   --no-compact \n"
  ^ "   --dump \n"
  ^ "   --ireclim number \n"
;;

let _args_parse =
  let opts =
    [ "--in", Arg.Set_string infile, ": tplname file"
    ; "--out", Arg.Set_string outfile, ": tplname file"
    ; "--stdout", Arg.Set to_stdout, ": tplname file"
    ; "--vars", Arg.Set_string varsfile, ": varsfile file"
    ; "--no-pretty", Arg.Set compact, ": pretty print"
    ; "--no-compact", Arg.Set pretty, ": compact print"
    ; "--dump", Arg.Set dump, ": dump various infos"
    ; "--ireclim", Arg.Set_int ireclim, ": include recursion limit"
    ]
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let open_vars varsfile =
  if Sys.file_exists varsfile
  then
    let open Otoml in
    let toml = Parser.from_file varsfile in
    let table = find toml get_table [ "variables" ] in
    List.fold_left
      (fun acc (key, value) -> (key, Utils.coerce_value (get_string value)) :: acc)
      []
      table
  else []
;;

let () =
  if Sys.file_exists !infile
  then (
    let outfile =
      if !outfile = String.empty
      then
        Printf.sprintf
          "%s-rendered%s"
          (Filename.chop_extension !infile)
          (Filename.extension !infile)
      else !outfile
    in
    let vars = if !varsfile = String.empty then [] else open_vars !varsfile in
    Tmpl.render_file
      ~infile:!infile
      ~outfile
      ~pretty:!pretty
      ~compact:!compact
      ~reclimit:(if !ireclim <= 0 then def_ireclim else !ireclim)
      ~vars
      ~dump:!dump
      ~to_stdout:!to_stdout
      ())
  else Printf.printf "%s\n" usage
;;
